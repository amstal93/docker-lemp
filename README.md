<div align="center">
<h1>
<br />
<br />
![hcrGX8b.png](https://i.imgur.com/hcrGX8b.png)
<br />
<br />
<br />
</h1>
<br />
</div>
<div align="center">
<h4 align="center">LEMP development envirement using <a href="https://www.docker.com/" target="_blank">Docker</a>.</h4>
<a href="#about">About</a> • <a href="#packages-versions">Packages versions</a> • <a href="#usage">Usage</a> • <a href="#author">Author</a> • <a href="#license">License</a>
</div>
<br />

## About

Are you tired of using MAMP, WAMP, LAMP, etc? A good alternative is Docker. It's easier to manage, it's easier to control, and there are no limits.
LEMP Docker is based on NGINX, PHP 7.3, MariaDB, phpMyAdmin, and Mailhog. You can customize Nginx configuration inside ./nginx/nginx.conf, you can do the same for PHP configuration inside the dockerfile. Support for non-ssl, and SSL protocol (self-signed).
<br />

## Packages versions
<a href="https://hub.docker.com/_/nginx">NGINX Mainline</a>  •  <a href="https://github.com/docker-library/php/blob/b909ae3f886865d5144fc344b6c755cafbd95715/7.3/alpine3.10/fpm/Dockerfile">PHP-FPM 7.3 - alpine</a>  •  <a href="https://hub.docker.com/_/mariadb">MariaDB latest</a>  •  <a href="https://hub.docker.com/r/phpmyadmin/phpmyadmin/">phpMyAdmin</a>  •  <a href="https://hub.docker.com/r/mailhog/mailhog/">Mailhog</a>
<br />

### PHP Versions

I tested multiple PHP versions as listed below. 

| PHP Version | OS Alpine  |
|:---------------:|:-------------:|
| **5.6 FPM**  | ✓  |
| **7.0 FPM**    | ✓      |
| **7.1 FPM**   | ✓     |
| **7.2 FPM**  | ✓  |
| **7.3 FPM**  | ✓  |

### Supported OS

| | |
|-|-|
| **Windows 10**  | ✓  |
| **MacOS**    | ✓      |
| **Linux**   | ✓     |

## Usage

### Prerequisites

You need to have Docker installed. If you dont have it, you can grab it <a href="https://www.docker.com/get-started" target="_blank">here</a>


### Installation

You can download this project in two ways. 

**Get the zip file - [Download Docker LEMP .zip](https://gitlab.com/paveljame/docker-lemp/-/archive/master/docker-lemp-master.zip).**

or just clone repository
```sh
git clone git@gitlab.com:paveljame/docker-lemp.git
```
### Controling containers - basic control

Starting containers
```sh
docker-compose up -d
```

Stoping containers
```sh
docker-compose stop
```

Stoping and destroying the containers
```sh
docker-compose down
```

### .env Configuration
Basic settings of the project can be changed inside the .env file. 

**.env file containes variables for:**

| Variable name| Variable Default Value| Description  |
|:---------------:|:-------------:|:-------------:|
| PROJECT    | dev      | Project name |
| USER_ID   | 1000      | User ID for container |
| GROUP_ID   | 1000      | Group ID for container |
| PHP_VERSION   | 7.3     | PHP Version |
| DB_NAME   | database    | Database name |
| DB_USER   | user    | Database user |
| DB_USER_PASSWORD   | password    | User password|
| DB_ROOT_PASSWORD   | password    | Root password |
| DB_PORT   | 3306    | MySQL port |
| HTTP_PORT  | 80 | HTTP Port for NGINX container |
| HTTPS_PORT  | 443 | HTTPS Port for NGINX ccontainer |
| PHPMYADMIN_PORT  | 8181  | phpMyAdmin port |
| SMTP_GUI  | 8025  | MailHog GUI port |


### File structure

```sh
├── nginx
│   ├── cert.pem # SSL Cert
│   ├── key.pem # SSL Key
│   └── nginx.conf # Basic Nginx configuration
│
├── php-docker
│   ├── permissions.sh # Script to autogenerate env variables for user and group ID
│   ├── php-*_*.dockerfile # PHP dockerfiles for different versions with added addons
│   └── cron # Files needed for enabling cronjob
│       ├── crontab.txt # Your crontab file
│       └── supervisord.conf # Supervisor file that enables running cron and PHP services at the same time
├── logs
│   ├── nginx # NGINX logs
│   │   ├── access.log
│   │   └── error.log
│   ├── cron # cron log
│   │   └── cron.log
│   └── supervisord # supervisor log
│       └── supervisord.log
├── mysql # You can place SQL files here if you want to import them
│   └── dump.sql
│
├── www # Project files
│   └── index.php
│
├── .env
│
├── docker-compose.yml
│
└── README.md 
```

### Links
| URL  | Description  |
|:---------------:|:-------------:|
| [http://localhost](http://localhost) <br> [https://localhost](http://localhost)   | **Web** |
| [http://localhost:8181](http://localhost:8181)  | **phpMyAdmin** |
| [http://localhost:8025](http://localhost:8025) | **Mailhog**  |

### Ports

| Port | Description  |
|:---------------:|:-------------:|
| **80**    | http      |
| **443**   | https     |
| **3306**  | Database  |
| **8181**  | Phpmyadmin|
| **25**    | SMTP      |
| **8025**  | Mailhog   |


## Specific setups

#### If you are using Linux as host

If you are using Linux as host os, and you have some issues with the php app, then you will need to add your user id and group id into .env file. You can do that in two ways:

**Running our script** 
To run our script you just need to be in the root of the project, and enter this command in your terminal: 

```sh
sh ./php-docker/permissions.sh   
```
or

**Enter user id and group id manually**

Open .env file and you will see two variables **USER_ID** and **GROUP_ID**. To find out your user ID and group ID, you will need to enter ```id -u``` for user ID, and ```id -g``` for group ID into your terminal

```sh
# Permission settings (your user and group IDs)
#***************
USER_ID=1000
GROUP_ID=1000 
```

**Update docker-compose file**

After that go inside the docker-compose.yml and uncomment ```# user: "${USER_ID}:${GROUP_ID}"``` inside the php services.


#### How to change PHP version?

Inside the .env file, you can find a section

```sh
    # PHP version
    # Add PHP version in the same style as shown bellow (7.3, 5.6, 7, etc)
    #**************
    PHP_VERSION=7.3
```
The default version of PHP is 7.3, but you can change it to any of the listed above. If you want to change the PHP version to 7.1, just make sure that this part of the .env file looks like this:

```sh
# PHP version
# Add PHP version in the same style as shown below (7.3, 5.6, 7, etc)
#**************
PHP_VERSION=7.1
```

If you change those settings before you run for the first time command ``` docker-compose up ``` then you don't need to do anything else, but if you make that change after you run that command you will need to run a command like this:

```sh
docker-compose up -d --force-recreate --build
```
All dockerfiles are located inside **php-docker **folder.

#### Using cron job inside the PHP container

In case you want to use a **cronjob inside the PHP container**, you can do this with the help of a dockerfile template located within the php-docker folder (**php-7.3-cron.dockerfile**). 

There is currently only one template for PHP version 7.3 and you can activate it immediately within the .env file:

```sh
# PHP version
# Add PHP version in the same style as shown below (7.3, 5.6, 7, etc)
#**************
PHP_VERSION=7.3-cron

```

If you want a cronjob option on another version of PHP, just copy the existing dockerfile of the desired PHP version and add the part that applies to cron and supervisor.

```sh

# Create cron log files 
RUN mkdir -p /var/log/cron \
 && touch /var/log/cron/cron.log \
 && touch /var/log/cron/crontab.log 

# Copy our cronjob list
COPY ./cron/crontab.txt /cronjob.txt
RUN	crontab /cronjob.txt

# Copy our supervisor config file
COPY ./cron/supervisord.conf /etc/supervisor/supervisord.conf

# Let's go
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

```

Don't forget to remove ``` CMD ["php-fpm"] ``` from the new template, becaouse supervisior is taking care for that part.

Custom cronjobs can be defined inside the php-docer/cron/crontab.txt.
Supervisor (the tool that enables us to run multiple services in one container) config file is located inside the  php-docer/cron/supervisord.conf

#### Using other special options inside the PHP container

In case there is a need to install special tools and php plugins, I have created additional predefined options within the dockerfile to install them. There is currently only one, but I will add more over the time. 

**How to enable the installation of additional tools**

Open the dockerfile of your chosen PHP version, and on the top of the file you will see ARG defined:

```sh
FROM php:7.3-fpm-alpine
# You can add options for image instalation. See readme for more info
ARG OPTIONS=no
```

If you want to enable additional installation, just insert yes as an argument

```sh
FROM php:7.3-fpm-alpine
# You can add options for image instalation. See readme for more info
ARG OPTIONS=yes
```

**Existing options:**

**Wordpress:** Wordpress install will add new packages: ``` curl, mysql-client, less, zlib ``` and will enable php plugins: ``` pdo, pdo_mysql, mysqli, gd ```. Main tool that we are installing is [Wordpress CLI](https://wp-cli.org/). If you want to enable this option, just use ``` ARG wordpress=yes ```

### Other Tips

**Starting docker-compose with active logs shown in terminal**

Run containers with active output (showing logs and if you want to stop containers you need to press ctrl + c)
```sh
docker-compose up
```
----------
**Destroying containers and volumes**

By default, with ```docker-compose down```, the only things removed are:

- Containers for services defined in the Compose file
- Networks defined in the networks section of the Compose file
- The default network, if one is used

<a href="https://docs.docker.com/compose/reference/down/" target="_blank">Source</a>

If you want to remove volumes data too, just use:
```sh
docker-compose down -v
```

----------
**SSH to individual container**

If you want to have ssh access to some of the containers just use this template:
```sh
docker exec -it name-of-container /bin/bash
```
For example, if you want to have access to Nginx container with the default project name "dev":
```sh
docker exec -it dev_nginx /bin/bash
```
*NOTE: Alpine version of containers (in our case PHP and maihlog containers), you need to start this command with ``` /bin/ash ``` insted of ``` /bin/bash ```*

-----
**Getting IP address of individual container**

If you want to see the IP address of single container use this template:
```sh
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' name-of-container
```
For example, if you want to see the Nginx container IP address with the default project name "dev":
```sh
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dev_nginx
```

-----
**Changing dockerfile**

If you change the dockerfile after you already run ```docker-compose up```, you will need to run this command if you want to build a new image
```sh
docker-compose build
```

or if you want to force new build and starting containers at the same time: 
```sh
docker-compose up -d --force-recreate --build
```

-----
**Connecting to the database**

If you want to connect to the database, just use **db** (sometimes with added **:3306**) as host, not localhost!

<br />

## Author

<a href="https://pavelja.me"><img src="https://pavelja.me/assets/images/paveljame.svg" alt="Paveljame" width="200"></a>

**Bruno Pavelja** <br />
[LinkedIn](https://www.linkedin.com/in/brunopavelja/) / [Twitter](https://twitter.com/Bruno_AFK) / [Gitlab](https://gitlab.com/bruno-afk) / [Github](https://github.com/BrunoAFK) / [Paveljame](https://pavelja.me)

## License

This project is using [MIT license](https://gitlab.com/paveljame/docker-lemp/blob/master/LICENSE)
